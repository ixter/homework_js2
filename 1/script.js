'use strict'

/**
 *  функция создает контейнер получая параметры айди и класснейм
 * @param id
 * @param className
 * @constructor
 */
function Container(id, className) {
    this.id = id;
    this.class = className;
};

// Container.prototype.render = function() {
//   var div = document.createElement('div');
//
//   div.className = this.className;
//   div.id = this.id;
//
//   return div;
// };

/**
 *  Функция меню получает 3 параметра и вызывает контейнер с 2 параметрами и количествой айтемов
 * @param id
 * @param className
 * @param items
 * @constructor
 */
function Menu(className, id, items) {
    Container.call(this);
    this.items = items;
    this.className = className;
    this.id = id;
};


// создается меню из прототипа контейнера.
Menu.prototype = Object.create(Container.prototype);

/**
 *  в меню задаем переменную ul и в ней создаем html элемент.
 *  в переменную ул закидываем параметры айди и класса из контейнера
 *  затем перебираем айтемы с проверкой на из пренадлежность к контенеру
 *  и аппендим их в ул айтемы
 *  затем возвращаем ул
 * @return {HTMLUListElement}
 */
Menu.prototype.render = function() {
    var menu = document.createElement('ul');
    menu.className = this.class;
    menu.id = this.id;

    this.items.forEach(function(item) {
        if (item instanceof Container) {
            menu.appendChild(item.render());
        }
    });

    return menu;
};

/**
 *  Функция получает 2 параметра ссылка и название
 *  вызывается контейнер с параметрами
 * @param href
 * @param label
 * @constructor
 */
function MenuItem(href, label) {
    Container.call(this, '', 'menu-item');

    this.href = href;
    this.label = label;
};

// меню айтем прототип --> скопировать объект контейнер пототип
MenuItem.prototype = Object.create(Container.prototype);

/**
 *  ренедере меню айтем
 *  создаем 2 переменные
 * ли и а
 * задаем переменной а ссылку и заголовок
 * аппендим в ли полученный элемент а
 * задаем классу ли класс
 * возвращаем ли
 * @return {HTMLLIElement}
 */
MenuItem.prototype.render = function() {
    var li = document.createElement('li');
    var a = document.createElement('a');

    a.href = this.href;
    a.textContent = this.label;

    li.appendChild(a);
    li.className = this.class;

    return li;
};

function Undermenu(className, id, items, title , href){
    Menu.call(this, className, id, items);
    this.title = title;
    this.href = href;
};

Undermenu.prototype = Object.create(Menu.prototype);

Undermenu.prototype.render = function() {
    if(this.title && this.href){
        var menuItem = new MenuItem(this.href, this.title).render();
        menuItem.appendChild(Menu.prototype.render.call(this));
        return menuItem;
    }else{
        return Menu.prototype.render.call(this);
    }
};

// мой метод remove
Container.prototype.remove = function () {
    document.querySelector('menu').remove(); //Метод удаляет меню

    // 2 Вариант ремова
    // var node = document.getElementById(this.id);
    //
    // node.parentElement.removeChild(node);
};
function remove() {
    menu.remove();
    console.log('remove');
};

// забиваем в переменные данные меню
var items = [
    new MenuItem('/', 'Home'),
    new MenuItem('/news', 'News'),
    new MenuItem('/blog', 'Blog'),
    new Undermenu('subMenu', 'subMenu',[
        new MenuItem('/man', 'Mans wear'),
        new MenuItem('/woman', 'Womans wear'),
        new MenuItem('/children', 'Childrens wear'),
    ], 'Underwear', '/underwear')
];
var menus = new Undermenu('menu', 'menu', this.items);

// Отрисовываем по нажатию кнопки.
function createMenu() {
    document.body.appendChild(menus.render());
}
//
// window.onload = function() {
//
//
// };
