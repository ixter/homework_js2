/**
 * Класс, объекты которого описывают параметры гамбургера.
 *
 * @constructor
 * @param size        Размер
 * @param stuffing    Начинка
 * @throws {HamburgerException}  При неправильном использовании
 */
function Hamburger(size, stuffing) {
    /* Размеры, виды начинок и добавок */
    switch (size) {
        // Hamburger.SIZE_SMALL
        case 1 :
            this.price = 50;
            this.calories = 20;
            break;

        // Hamburger.SIZE_LARGE
        case 2 :
            this.price = 100;
            this.calories = 40;
            break;
        default:
            exeption('Неправильный размер');
    }
    /*   начинка               */

    switch (stuffing) {
        // Hamburger.STUFFING_CHEESE
        case 1:
            this.price += 10;
            this.calories += 20;
            break;
        // Hamburger.STUFFING_SALAD
        case 2:
            this.price += 20;
            this.calories += 5;
            break;
        // Hamburger.STUFFING_POTATO
        case 3:
            this.price += 15;
            this.calories += 10;
            break;
        default:
            exeption('Неправильная начинка');
    }
    this.toppingsS = [];
    this.sizeS = size;
    this.stuffingS = stuffing;
    // console.log('s1' + this.price);
}

/**
 * Добавить добавку к гамбургеру. Можно добавить несколько
 *– при условии, что они разные.
 *
 * @param topping     Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.addTopping = function (topping) {

    switch (topping) {
        // Hamburger.TOPPING_MAYO
        case 1:
            if (this.toppingsS.indexOf(Hamburger.TOPPING_MAYO) == -1) {
                this.toppingsS.push(Hamburger.TOPPING_MAYO);
                this.price += 15;
                this.calories += 0;
            }
            break;
        // Hamburger.TOPPING_SPICE
        case 2:
            if (this.toppingsS.indexOf(Hamburger.TOPPING_SPICE) == -1) {
                this.toppingsS.push(Hamburger.TOPPING_SPICE);
                this.price += 20;
                this.calories += 5;
            }
            break;
        default:
            exeption('Неправильный топинг');
    }
    // console.log('t'+this.price);
}


Hamburger.TOPPING_MAYO = 1;
Hamburger.TOPPING_SPICE = 2;


/**
 * Убрать добавку – при условии, что она ранее была
 * добавлена.
 *
 * @param topping   Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.removeTopping = function (topping){
    switch (topping){
        case 1:
            if (this.toppingsS.indexOf(Hamburger.TOPPING_MAYO) >= 0){
                this.toppingsS.splice(this.toppingsS.indexOf(Hamburger.TOPPING_MAYO));
                this.price -= 15;
                this.calories -= 0;
            }
            break;
        case 2:
            if (this.toppingsS.indexOf(Hamburger.TOPPING_SPICE) >= 0) {
                this.toppingsS.splice(this.toppingsS.indexOf(Hamburger.TOPPING_SPICE));
                this.price += 20;
                this.calories += 5;
            }
            break;
        default:
            exeption('Неправильный топинг');
    }
}


/**
 * Получить список добавок.
 *
 * @return {Array} Массив добавленных добавок, содержит константы
 *                 Hamburger.TOPPING_*
 */
Hamburger.prototype.getToppings = function (){
 return this.toppingsS;
}
/**
 * Узнать размер гамбургера
 */
Hamburger.prototype.getSize = function (){
    return this.sizeS;
}

/**
 * Узнать начинку гамбургера
 */
Hamburger.prototype.getStuffing = function (){
 return this.stuffingS;
}

/**
 * Узнать цену гамбургера
 * @return {Number} Цена в тугриках
 */
Hamburger.prototype.calculatePrice = function (){
    // console.log('cP'+this.price);
    return this.price;
}
/**
 * Узнать калорийность
 * @return {Number} Калорийность в калориях
 */
Hamburger.prototype.calculateCalories = function (){
    return this.calories;
}

/**
 * Представляет информацию об ошибке в ходе работы с гамбургером.
 * Подробности хранятся в свойстве message.
 * @constructor
 */
function exeption(message) {
    alert(message);
}

window.onload = function () {

    document.getElementById('form').addEventListener('submit', function (e) {

        var burger = new Hamburger(+this.size.value, +this.stuffing.value);

        if (this.mayo.checked) {
            burger.addTopping(1)
        }

        if (this.spice.checked) {
            burger.addTopping(2)
        }

        document.getElementById("price").innerHTML = "Стоимость " + burger.calculatePrice() + " руб"
        document.getElementById("calories").innerHTML = "Калории " + burger.calculateCalories() + " ккал"

        e.preventDefault();
    });
}