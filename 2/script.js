'use strict'

/**
 *  функция создает контейнер получая параметры айди и класснейм
 * @param id
 * @param className
 * @constructor
 */
function Container(id, className) {
    this.id = id;
    this.class = className;
};

// Container.prototype.render = function() {
//   var div = document.createElement('div');
//
//   div.className = this.className;
//   div.id = this.id;
//
//   return div;
// };

/**
 *  Функция меню получает 3 параметра и вызывает контейнер с 2 параметрами и количествой айтемов
 * @param id
 * @param className
 * @param items
 * @constructor
 */
function Menu(className, id, items) {
    Container.call(this);
    this.items = items;
    this.class = className;
    this.id = id;
};


// создается меню из прототипа контейнера.
Menu.prototype = Object.create(Container.prototype);

/**
 *  в меню задаем переменную ul и в ней создаем html элемент.
 *  в переменную ул закидываем параметры айди и класса из контейнера
 *  затем перебираем айтемы с проверкой на из пренадлежность к контенеру
 *  и аппендим их в ул айтемы
 *  затем возвращаем ул
 * @return {HTMLUListElement}
 */
Menu.prototype.render = function() {
    var menu = document.createElement('ul');
    menu.className = this.class;
    menu.id = this.id;

    this.items.forEach(function(item) {
        if (item instanceof Container) {
            menu.appendChild(item.render());
        }
    });

    return menu;
};

/**
 *  Функция получает 2 параметра ссылка и название
 *  вызывается контейнер с параметрами
 * @param href
 * @param label
 * @constructor
 */
function MenuItem(href, label, className) {
    if(className){
        Container.call(this, '', 'pure-menu-item pure-menu-selected pure-menu-has-children pure-menu-allow-hover');
    }else {
    Container.call(this, '', 'pure-menu-item pure-menu-selected');
    }
    this.href = href;
    this.label = label;
};

// меню айтем прототип --> скопировать объект контейнер пототип
MenuItem.prototype = Object.create(Container.prototype);

/**
 *  ренедере меню айтем
 *  создаем 2 переменные
 * ли и а
 * задаем переменной а ссылку и заголовок
 * аппендим в ли полученный элемент а
 * задаем классу ли класс
 * возвращаем ли
 * @return {HTMLLIElement}
 */
MenuItem.prototype.render = function() {
    var li = document.createElement('li');
    var a = document.createElement('a');

    a.href = this.href;
    a.textContent = this.label;
    a.className = 'pure-menu-link';

    li.appendChild(a);
    li.className = this.class;

    return li;
};

function Undermenu(className, id, items, title , href){
    Menu.call(this, className, id, items);
    this.title = title;
    this.href = href;
};

Undermenu.prototype = Object.create(Menu.prototype);

Undermenu.prototype.render = function() {
    if(this.title && this.href){
        var menuItem = new MenuItem(this.href, this.title, this.class).render();
        menuItem.appendChild(Menu.prototype.render.call(this));
        return menuItem;
    }else{
        return Menu.prototype.render.call(this);
    }
};

// мой метод remove
Container.prototype.remove = function () {
    document.querySelector('menu').remove(); //Метод удаляет меню

    // 2 Вариант ремова
    // var node = document.getElementById(this.id);
    //
    // node.parentElement.removeChild(node);
};
function remove() {
    menu.remove();
    console.log('remove');
};

// забиваем в переменные данные меню
var items = [
    new MenuItem('/', 'Home'),
    new MenuItem('/news', 'News'),
    new MenuItem('/blog', 'Blog'),
    new Undermenu('pure-menu-children pure-menu-has-children', 'subMenu',[
        new MenuItem('/man', 'Mans wear'),
        new MenuItem('/woman', 'Womans wear'),
        new MenuItem('/children', 'Childrens wear'),
        new Undermenu('pure-menu-children pure-menu-has-children', 'subMenu',[
            new MenuItem('/man', 'Mans wear'),
            new MenuItem('/woman', 'Womans wear'),
            new MenuItem('/children', 'Childrens wear'),
            new Undermenu('pure-menu-children pure-menu-has-children', 'subMenu',[
                new MenuItem('/man', 'Mans wear'),
                new MenuItem('/woman', 'Womans wear'),
                new MenuItem('/children', 'Childrens wear'),
            ], 'Underwear', '/underwear')
        ], 'Underwear', '/underwear'),
        new Undermenu('pure-menu-children pure-menu-has-children', 'subMenu',[
            new MenuItem('/man', 'Mans wear'),
            new MenuItem('/woman', 'Womans wear'),
            new MenuItem('/children', 'Childrens wear'),
            new Undermenu('pure-menu-children pure-menu-has-children', 'subMenu',[
                new MenuItem('/man', 'Mans wear'),
                new MenuItem('/woman', 'Womans wear'),
                new MenuItem('/children', 'Childrens wear'),
            ], 'Underwear', '/underwear')
        ], 'Underwear', '/underwear')
    ], 'Underwear', '/underwear'),
    new Undermenu('pure-menu-children pure-menu-has-children', 'subMenu',[
        new MenuItem('/man', 'Mans wear'),
        new MenuItem('/woman', 'Womans wear'),
        new MenuItem('/children', 'Childrens wear'),
    ], 'Underwear', '/underwear')
];
var menus = new Undermenu('pure-menu-list pure-menu-has-children', 'menu', this.items);

// Отрисовываем по нажатию кнопки.
function createMenu() {
    document.querySelector('menu').appendChild(menus.render());
}


/**
 *
 *
 *
 *   галлерея
 *
 *   https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest/response
 *
 *
 *
 */

/**
 * функция выполняет аякс запрос и получает ответ в качестве урл запрашивается фаил img.json
 * @param callback
 */
function getImg(callback){
    var xhr = new XMLHttpRequest(),
        method = "GET",
        url = "img.json";

    xhr.open(method, url, true);
    // xhr.responseType = 'json';
    xhr.onreadystatechange = function () {
        if(xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
            // console.log(xhr.responseText);
            callback(JSON.parse(xhr.responseText));

        };
    };
    xhr.send();
}

window.onload = function() {

    /**
     *
     * функция запрашивает через аякс запрос картинки и выводит их
     *
     * создаем div
     * в div запихиваем a ---> img
     *
     * отрисовываем элементы с данными
     */
    getImg(function (imgList) {
        var main = document.createElement('div');
        main.className = 'pure-g';
        imgList.img.forEach(function (item) {
            var div = document.createElement('div');
            div.className = 'pure-u-1-4 pure-u-lg-1-8';
            var a = document.createElement('a');
            var img = document.createElement('img');
            img.className = 'pure-img';
            a.href = item.big;
            img.src = item.small;
            a.appendChild(img);
            div.appendChild(a);
            main.appendChild(div);
        });
        document.querySelector('gallery').appendChild(main);
        // console.log(response);

    })


};

/**
 *
 *
 *   result
 *
 *
 *
 *
 */


/**
 * функция выполняет аякс запрос и получает ответ в качестве урл запрашивается фаил result.json или resultS.json
 * @param url
 */
function getAnswer(url){
    var xhr = new XMLHttpRequest(),
        method = "GET";
    xhr.open(method, url, true);
    // xhr.responseType = 'json';
    xhr.onreadystatechange = function () {
        if(xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {

            if(JSON.parse(xhr.responseText).result === 'success'){
                // console.log('suces');
                document.getElementById('resultS').classList.add('success');
                document.getElementById('resultF').classList.remove('fail');
            }else {
                // console.log('false');
                document.getElementById('resultS').classList.remove('success');
                document.getElementById('resultF').classList.add('fail');
            };
        };
    };
    xhr.send();
}






