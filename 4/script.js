'use strict'

/**
 * регулярка проверяет введено ли число
 * @param item
 * @return {boolean}
 */
function number(item) {
    return /[0-9]/g.test(item);
};

/**
 * регулярка проверяет введена ли буква или слово
 * @param item
 * @return {boolean}
 */
function word(item) {
    return /(^[a-z]+$)|(^[а-я]+$)/gi.test(item);
};

/**
 *  регулярка проверяет введен ли телефон по маске
 * @param item
 * @return {boolean}
 */
function phoneV(item) {
    return /^\+\d\(\d{3}\)\d{3}-\d{4}$/.test(item);
// ** - Телефон подчиняется шаблону +7(000)000-0000;**
};

/**
 *  регулярка проверяет введен ли емеил по маске
 * @param item
 * @return {boolean}
 */
function mailV(item) {
    return /(^\w+@[a-zA-Z_]+?\.[ru]{2,6}$)/.test(item);
    // return /(\w+@[a-zA-Z_]+?\.[a-zA-z]{2,6})/.test(item);
}

/**
 *  функция выводит ошибки если данные не валидны
 * @param elem
 * @param error
 * @param valid
 */
function error(elem, error, valid) {
    var getElem = document.getElementsByClassName(elem)[0];
    if (valid === '' || valid === undefined) {
        if (getElem.querySelector('.error') != null) {


        } else {
            var div = document.createElement('div');
            div.className = 'error';
            var span = document.createElement('span');
            span.className = 'error_text';
            span.textContent = error;
            div.appendChild(span);
            // getElem.className = 'errorB';
            getElem.classList.add('errorB');
            getElem.appendChild(div);
        }
    } else {
        if (getElem.querySelector('.error') != null) {
            getElem.classList.remove('errorB');
            getElem.querySelector('.error').remove();
        }
    }
};

/**
 *  функция проверки данных
 * @param name
 * @param phone
 * @param email
 * @return {boolean}
 */
function validate(name, phone, email) {
    var nameV = false;
    var phoneVV = false;
    var emailV = false;

    if (number(name)) {
        // console.log('true number');
        error('name', 'Ошибка! Неправильно введено поле Имя');
        nameV = false;
    } else if (word(name)) {
        // console.log('true word');
        error('name', '', true);
        nameV = true;
    } else {
        // console.log('else');
        error('name', 'Ошибка! Неправильно введено поле Имя');
        nameV = false;
    }

    if (phoneV(phone)) {
        // console.log(phoneV(phone)+ ' phone');
        error('tel', '', true);
        phoneVV = true;
    } else {
        // console.log(phoneV(phone)+ ' phone');
        error('tel', 'Ошибка! Неправильно введено поле email');
        phoneVV = false;
    }

    if (mailV(email)) {
        // console.log(mailV(email)+ ' email');
        error('email', '', true);
        emailV = true;
    } else {
        // console.log(mailV(email)+ ' email');
        error('email', 'Ошибка! Неправильно введено поле email');
        emailV = false;
    }

    if (nameV === true && phoneVV === true && emailV === true) {
        return true;
    } else {
        return false;
    }
    // if((/^\w{1,25}$/gi).test(name) == true){
    //     console.log('true');
    // }else {
    //     console.log('else');
    // }


}

window.onload = function () {

    /**
     *  следит за изменениями на форме и проверяет валидность введенных данных
      */
    document.getElementById('form2').addEventListener('change', function (e) {
        e.preventDefault();
        var name = this.name.value,
            phone = this.phone.value,
            email = this.email.value,
            text = this.text.value;
        validate(name, phone, email);
    });

    /**
     *  при отправке формы проверяет валидность и если всё ок выводит данные.
     */
    document.getElementById('form2').addEventListener('submit', function (e) {
        e.preventDefault();
        var name = this.name.value,
            phone = this.phone.value,
            email = this.email.value,
            text = this.text.value;
        var vailide = validate(name, phone, email);
        if (vailide === true) {
            document.getElementById("result").innerHTML = "Ваши Данные: " + name + "</br>" + "Телефон" + phone + "</br>" + "Email " + email + "</br>" + "Текст " + text;
        } else {
            document.getElementById("result").innerHTML = "На форме найдены ошибки";
        }
    });


    (function($) {
        var tab_block = $( "#tab__block" );
        var controlbtns = $('.control-btn');
        var controlbtabs = $('div .tab');
        $(function() {
            controlbtns.on('click', function (e) {
                var index = controlbtns.index( this );
                setActive(index);
                // console.log(index);
            });


        });
        function setActive(index)
        {
            // controlbtabs.index(index).toggleClass('active', true);
            // var aaaa = controlbtabs.eq(index);
            tab_block.find( ".tab" ).siblings().removeClass('active');
            tab_block.find( controlbtns ).siblings().removeClass('active');
            tab_block.find( ".tab" ).eq( index ).addClass('active');
            tab_block.find( controlbtns ).eq( index ).addClass('active');
            // console.log('set active '+ index);
        }

        var getElemCity = document.getElementById('city');

        $.ajax({
            url: 'http://localhost:3000/citys',
            dataType : "json",
            success: function (data, textStatus) {
                $.each(data, function(i, val) {
                    var option = document.createElement('option');
                    option.textContent = val.city;
                    getElemCity.appendChild(option);
                });
            }
        });




    })(jQuery);

};


